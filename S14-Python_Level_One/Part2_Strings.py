
'hello'


'This is also a string'

"String built with double quotes"


# ' I'm using single quotes, but will create an error'



"Now I'm ready to use the single quotes inside a string!"


'Hello World'

'Hello World 1'
'Hello World 2'



print('Hello World 1')
print('Hello World 2')
print('Use \n to print a new line')
print('\n')
print('See what I mean?')



len('Hello World')


s = 'Hello World'

s


print(s)

s[0]

s[1]

s[2]



s[1:]

s


s[:3]


s[:]

s[-1]

s[:-1]


s[::1]


s[::2]

s[::-1]



s

s[0] = 'x'


s

s + ' concatenate me!'

s = s + ' concatenate me!'

print(s)



letter = 'z'

letter*10


s.upper()

s.lower()


s.split()


s.split('W')


'Insert another string with curly brackets: {}'.format('The inserted string')




print('This is a string with an {p}'.format(p='insert'))

print('One: {p}, Two: {p}, Three: {p}'.format(p='Hi!'))


print('Object 1: {a}, Object 2: {b}, Object 3: {c}'.format(a=1,b='two',c=12.3))


